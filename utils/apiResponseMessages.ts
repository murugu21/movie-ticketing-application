/* 
Response messages for auth,user controllers and middlewares
*/
export const NO_TOKEN_FOUND = "requried authentication was not sent";
export const INVALID_AUTHENTICATION = "Not authenticated";
export const NOT_OPERATOR = "You cannot create / update/ delete a bus";
export const INVAILD_ACCESS_OF_BUS =
  "access denied while accessing bus details ";
export const INVAILD_ACCESS_OF_TRIP =
  "access denied while accessing trip details ";
export const DUPLICATE_USER = "user already exists";
export const USER_CREATED = "user created";
export const LOGIN_SUCCESS = "Login successfull";
export const LOGIN_FAILURE = "wrong email/password";
export const NO_USER_FOUND = "No user found";

/* 
Response messages for bus controllers and middlewares
*/
export const BUS_NOT_FOUND = "no bus in db";
export const DUPLICATE_BUS = "A bus with registartion number already exists";
export const BUS_CREATED = "bus registerted successfully";
export const BUS_UPDATED = "Updation successfull";
export const BUS_DELETED = "Deletion successfull";

/* 
Response messages for trip controllers and middlewares
 */

export const TRIP_NOT_FOUND = "Haven't registered any trips ";
export const TRIP_CREATED = "Trip registerted successfully";
export const TRIP_UPDATED = "Updation successfull";
export const TRIP_DELETED = "Deletion successfull";
export const NO_TRIP = "Cannot find trips for on";
export const DATES_OVERLAP = "Dates overlap with existing trip dates";
export const BOOKED_TRIP = "Cannot updated/delete since trip got booked";
export const INVALID_DATE = "Enter proper end/start date";
export const DUPLICATE_TRIP =
  "A trip already exits , please change start date/end date";
export const TRIP_ENDED = "Cannot book this trip as it has already ended";

/* 
Response messages for bookings controllers and middlewares
 */
export const BOOKING_NOT_FOUND = "Haven't booked ticket/s ";
export const INVALID_BOOKING_DATA = "Enter proper details : AGE/GENDER/NAME";
export const BOOKING_CREATED = "Booked  successfully";
export const BOOKING_UPDATED = "Updation successfull";
export const BOOKING_DELETED = "Deletion successfull";
export const TRIP_FULL = "Bus is full please select another bus";
export const SEAT_NOT_AVAILABLE = "seat is not available";
export const CREATOR = "you cannot book since you are the creator";
export const UPDATE_CANCELLED_TICKET = "Cannot update cancelled tickets";
export const CANCELLED_TICKET =
  "Cannot cancel this trip as it has already ended";
export const ERROR_BOOKING = "Error in booking";
