export const BAD_REQUEST = 400;
export const ACCESS_DENIED = 403;
export const NOT_FOUND = 404;
export const CREATED = 201;
export const SUCCESS = 200;
export const INTERNAL_SERVER_ERROR = 500;
