import { NextFunction, Request, Response } from "express";

export function wrapAsync(
  fn: (arg0: Request, arg1: Response, arg2: NextFunction) => Promise<any>
) {
  return function (req: Request, res: Response, next: NextFunction) {
    fn(req, res, next).catch(next);
  };
}

export function wrapAsyncMiddleware(
  fn: (
    arg0: Request,
    arg1: Response,
    arg2: NextFunction,
    arg3: string
  ) => Promise<any>
) {
  return function (
    req: Request,
    res: Response,
    next: NextFunction,
    id: string
  ) {
    fn(req, res, next, id).catch(next);
  };
}
