import { Bus } from "../models/bus.model";
import { IBus } from "./../interfaces";
/**
 * *Finds bus document in database based on busID
 * @param id busId based on which bus has to be found
 * @returns Bus document
 */
export const findBusById = async (id: string) => {
  const bus = await Bus.findById(id);
  return bus;
};
/**
 * *Finds bus document in database based on operatorID
 * @param id operator based on which bus has to be found
 * @returns Bus document
 */
export const findBusByOperatorId = async (id: string) => {
  const bus = await Bus.find({ operatorId: id });
  return bus;
};
/**
 * * Creates Bus document based on details
 * @param value bus details
 * @returns created Bus document
 */
export const createBusDB = async (value: IBus) => {
  const bus = await Bus.create(value);
  return bus;
};
/**
 * * Updates Bus document based on details
 * @param id bus id
 * @param objectToUpdate bus details to be updated
 * @returns updated bus
 */
export const updateBusById = async (id: string, objectToUpdate: IBus) => {
  const bus = await Bus.findByIdAndUpdate(id, objectToUpdate);
  return bus;
};
/**
 * *Delete bus based on busID
 * @param id busid
 * @returns true
 */
export const deleteBusById = async (id: string) => {
  await Bus.findByIdAndDelete(id);
  return true;
};
/**
 * * Finds bus that matches the registrationNumber
 * @param registrationNumber
 * @returns bus document
 */
export const checkExistingBus = async (registrationNumber: string) => {
  const bus = await Bus.findOne({
    registrationNumber: registrationNumber,
  });
  return bus;
};
