import { ITrip, IBooking, IBookedSeats } from "./../interfaces";
import { Trip } from "../models/trip.model";
import { FilterQuery, Types } from "mongoose";

/**
 * *Finds trip document in database based on tripId
 * @param id tripId based on which trip has to be found
 * @returns trip document
 */
export const findTripById = async (id: string) => {
  const trip = await Trip.findById(id);
  return trip;
};
/**
 * * Fetches Trip from database based on given filters
 * @param value Based on these filters trips are fetched from database
 * @returns Trip array that match the filters
 */
export const findTripBasedOnFilters = async (value: Partial<ITrip>) => {
  const { from, to, startDate, endDate, isSleeper, isAc } = value;
  const trip = await Trip.find({
    from: from,
    to: to,
    StartDate: { $gt: startDate, $lt: endDate },
    isSleeper: isSleeper,
    isAc: isAc,
  });
  return trip;
};
/**
 * * Creates Trip document based on details
 * @param value trip details
 * @returns created trip document
 */
export const createNewTrip = async (value: Partial<ITrip>) => {
  const trip = await Trip.create(value);
  return trip;
};
/**
 * * Updates Trip document based on details
 * @param objectToUpdate trip details
 * @returns updates trip document
 */
export const updateTrip = async (
  id: string,
  objectToUpdate: Partial<ITrip>
) => {
  const trip = await Trip.findByIdAndUpdate(id, objectToUpdate);
  return trip;
};
/**
 * * Updates Trip document when ticket is booked
 * @param id tripId
 * @param createdBooking A booking object that has to be inserted in bookingSchema of trip
 * @returns trip document
 */
export const updateTripOnBookingCreation = async (
  id: string,
  createdBooking: IBooking
) => {
  const trip = await Trip.findOneAndUpdate(
    {
      _id: new Types.ObjectId(id),
      availableSeats: { $all: createdBooking.seatNo },
    },
    {
      $push: { bookings: createdBooking },
      $pull: { availableSeats: { $in: createdBooking.seatNo } },
    }
  );
  return trip;
};
/**
 * * Updates Trip document when ticket is updated
 * @param bookingId Id of ticket to be udpated
 * @param availableSeats array of available seats on trip
 * @param bookingInfo New booking value
 * @param seatNo array of seatno
 * @returns trip document
 */
export const updateTripOnBookingUpdation = async (
  bookingId: string,
  availableSeats: string[],
  bookingInfo: Partial<IBooking> & IBookedSeats,
  seatNo: string[]
) => {
  const { noOfSeats, passangerNames, gender, age } = bookingInfo;
  const filter: FilterQuery<ITrip> = {
    "bookings._id": new Types.ObjectId(bookingId),
  };
  if (seatNo.length !== 0) filter.availableSeats = { $all: seatNo };
  return await Trip.findOneAndUpdate(filter, {
    $set: {
      availableSeats,
      "bookings.$.noOfBookedSeats": noOfSeats,
      "bookings.$.seatNo": bookingInfo.seatNo,
      "bookings.$.passangerNames": passangerNames,
      "bookings.$.gender": gender,
      "bookings.$.age": age,
    },
  });
};

/**
 * *Updates Trip document when ticket is deleted
 * @param bookingId Id of ticket to be udpated
 * @param bookedSeatNo array of seatno
 */
export const updateTripOnBookingDeletion = async (
  bookingId: string,
  bookedSeatNo: string[]
) => {
  await Trip.findOneAndUpdate(
    {
      "bookings._id": new Types.ObjectId(bookingId),
    },
    {
      $pull: { bookings: { _id: new Types.ObjectId(bookingId) } },
      $push: { availableSeats: { $each: bookedSeatNo } },
    }
  );
};
/**
 * *Deletes trip based on tripid
 * @param id TripId
 * @returns true
 */
export const deleteTripById = async (id: string) => {
  await Trip.findByIdAndDelete(id);
  return true;
};
/**
 * * Used to check if another trip exists on same start/end date
 * @param busId busId to filter trips
 * @param currentStartDate startDate
 * @param currentEndDate endate
 * @returns list of trips that matches start and end date
 */
export const checkExistingTrip = async (
  busId: string,
  currentStartDate: Date,
  currentEndDate: Date
) => {
  const trip = await Trip.findOne({
    busId: busId,
    $and: [
      {
        startDate: {
          $gte: currentStartDate,
          $lte: currentEndDate,
        },
      },
      {
        currentEndDate: {
          $gte: currentStartDate,
          $lte: currentEndDate,
        },
      },
    ],
  });
  return trip;
};
/**
 * * finds trips registerd by operator based on operatorId
 * @param userId OperatorId based on which trip was found
 * @returns trip document
 */
export const findTripByOperatorId = async (userId: string) => {
  const trips = await Trip.find({ operatorId: userId });
  return trips;
};
