import { IUser } from "../interfaces";
import { User } from "../models/user.model";

/**
 * * Finds user based on email id
 * @param email Email id of user
 * @returns user that matches given email id
 */
export const findUserByMail = async (email: string) => {
  const user = await User.findOne({ email: email }).exec();
  return user;
};

/**
 * *Finds user based on given userId
 * @param id userid
 * @returns user that matches the id
 */
export const findUserById = async (id: string) => {
  const user = await User.findById(id).select("-password");
  return user;
};

/**
 * * Created user based on provided input
 * @param value User information to create a new user
 * @returns created user
 */
export const createUser = async (value: IUser) => {
  const user = await User.create(value);
  return user;
};
