import { Types } from "mongoose";
import { Trip } from "../models/trip.model";

/**
 * *Returns a booking document based on booking id
 * @param id ID of booked ticket
 * @returns trip that matches the booking id
 */
export const findTripByBookingId = async (id: string) => {
  const trips = await Trip.aggregate([
    { $match: { "bookings._id": new Types.ObjectId(id) } },
    { $unwind: "$bookings" },
    { $match: { "bookings._id": new Types.ObjectId(id) } },
  ]);

  if (trips.length === 0) {
    return null;
  }
  return trips[0];
};
/**
 * *Returns all booking made by user
 * @param userId id of user
 * @returns bookings array
 */
export const findBookingsBasedOnUserId = async (userId: string) => {
  const bookings = await Trip.aggregate([
    { $match: { "bookings.userId": new Types.ObjectId(userId) } },
    { $project: { bookings: 1 } },
  ]);
  return bookings;
};
