import { Request, Response, NextFunction } from "express";
import { busRegistrationValidationSchema } from "./validation.schema";
import { checkExistingBus, findBusById } from "../dbLayer/bus.db";
import { BUS_NOT_FOUND, DUPLICATE_BUS } from "../utils/apiResponseMessages";
import {
  BAD_REQUEST,
  NOT_FOUND,
  ACCESS_DENIED,
} from "../utils/apiResponseStatusCode";

export const getBusById = async (
  req: Request,
  res: Response,
  next: NextFunction,
  id: string
) => {
  const bus = await findBusById(id);
  if (bus) {
    req.body.bus = bus;
    next();
  } else {
    return res.status(NOT_FOUND).json({
      error: BUS_NOT_FOUND,
    });
  }
};

export const busRegistrationSchemaValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = busRegistrationValidationSchema.validate(req.body);
  if (error) {
    return res.status(BAD_REQUEST).json({ error: error.details[0].message });
  }
  next();
};

export const isBusExists = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { registrationNumber } = req.body;

  const registeredBus = await checkExistingBus(registrationNumber);
  if (registeredBus) {
    return res.status(ACCESS_DENIED).json({
      error: DUPLICATE_BUS,
    });
  }
  next();
};

export const appendOperatorIdToBody = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const operatorId = req.body.auth.userId;
  req.body.operatorId = operatorId;
  next();
};
