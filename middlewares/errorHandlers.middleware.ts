import { NextFunction, Request, Response } from "express";
import { INTERNAL_SERVER_ERROR } from "../utils/apiResponseStatusCode";

export function handleMongooseError(
  error: unknown,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (error instanceof Error && error.name === "MongooseError") {
    return res.status(INTERNAL_SERVER_ERROR).json({
      error: error.message,
    });
  }
  next(error);
}

export function handleDatabaseError(
  error: unknown,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (error instanceof Error && error.name === "MongoServerError") {
    return res.status(INTERNAL_SERVER_ERROR).json({
      error: error.message,
    });
  }
  next(error);
}

export function handleDefaultError(
  error: unknown,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (error instanceof Error) {
    return res.status(INTERNAL_SERVER_ERROR).json({
      error: error.message,
    });
  }
  next(error);
}
