import { Request, Response, NextFunction } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import { SECRET_KEY } from "../config";
import { findUserById, findUserByMail } from "../dbLayer/auth.db";
import {
  DUPLICATE_USER,
  INVAILD_ACCESS_OF_BUS,
  INVAILD_ACCESS_OF_TRIP,
  INVALID_AUTHENTICATION,
  NOT_OPERATOR,
  NO_TOKEN_FOUND,
} from "../utils/apiResponseMessages";
import { BAD_REQUEST, ACCESS_DENIED } from "../utils/apiResponseStatusCode";
import {
  userRegistartionValidationSchema,
  loginValidationSchema,
} from "./validation.schema";

export const isSignedin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.header("Authorization");
  if (!token) {
    return res.status(BAD_REQUEST).json({
      error: NO_TOKEN_FOUND,
    });
  }

  const decode = jwt.verify(token, SECRET_KEY);
  req.body.auth = decode as JwtPayload;
  if (await isUserExists(req.body.auth.userId)) {
    next();
  } else {
    return res.status(ACCESS_DENIED).json({
      error: INVALID_AUTHENTICATION,
    });
  }
};

export const isOperator = (req: Request, res: Response, next: NextFunction) => {
  if (req.body.auth.type !== "operator") {
    return res.status(ACCESS_DENIED).json({
      error: NOT_OPERATOR,
    });
  }
  next();
};

export const isAuthenticatedToAccessBus = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const checker =
    req.body.bus &&
    req.body.auth &&
    String(req.body.bus.operatorId) === String(req.body.auth.userId);
  if (!checker) {
    return res.status(ACCESS_DENIED).json({
      error: INVAILD_ACCESS_OF_BUS,
    });
  }
  next();
};

export const isAuthenticatedToAccessTrip = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const checker =
    req.body.trip &&
    req.body.auth &&
    String(req.body.trip.operatorId) === String(req.body.auth.userId);
  if (!checker) {
    return res.status(ACCESS_DENIED).json({
      error: INVAILD_ACCESS_OF_TRIP,
    });
  }
  next();
};

export const registartionSchemaValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = userRegistartionValidationSchema.validate(req.body);
  if (error) {
    return res.status(BAD_REQUEST).json({ error: error.details[0].message });
  }
  next();
};

export const loginSchemaValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = loginValidationSchema.validate(req.body);
  if (error) {
    return res.status(BAD_REQUEST).json({ error: error.details[0].message });
  }
  next();
};

export const isExistingUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email } = req.body;
  const existingUser = await findUserByMail(email);

  if (existingUser) {
    return res.status(ACCESS_DENIED).json({
      error: DUPLICATE_USER,
    });
  }
  next();
};
/**
 * * Used to find duplicate user
 * @param id checks if user exists based on Id
 * @returns true if user exists
 */
export const isUserExists = async (id: string) => {
  const user = await findUserById(id);
  return user ? true : false;
};
