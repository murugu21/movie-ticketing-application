import { Request, Response, NextFunction } from "express";
import { tripRegistrationValidationSchema } from "./validation.schema";
import { findTripById, checkExistingTrip } from "../dbLayer/trip.db";
import {
  BOOKED_TRIP,
  DATES_OVERLAP,
  DUPLICATE_TRIP,
  INVALID_DATE,
  TRIP_NOT_FOUND,
} from "../utils/apiResponseMessages";
import { BAD_REQUEST, NOT_FOUND } from "../utils/apiResponseStatusCode";
export const getTripById = async (
  req: Request,
  res: Response,
  next: NextFunction,
  id: string
) => {
  const trip = await findTripById(id);
  if (trip) {
    req.body.trip = trip;
    next();
  } else {
    return res.status(NOT_FOUND).json({
      error: TRIP_NOT_FOUND,
    });
  }
};

export const tripRegistrationSchemaValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = tripRegistrationValidationSchema.validate(req.body);

  if (error) {
    return res.status(BAD_REQUEST).json({
      error: error.details[0].message,
    });
  }
  next();
};

export const doesTripExists = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const busId = req.body.bus.id;
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const trip = await checkExistingTrip(busId, startDate, endDate);
  if (trip) {
    return res.status(BAD_REQUEST).json({
      error: DATES_OVERLAP,
    });
  }

  next();
};

export const isSeatsBooked = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const trip = await findTripById(req.body.trip.id);
  if (!trip || trip.availableSeats.length < trip.totalSeats) {
    return res.status(BAD_REQUEST).json({
      error: BOOKED_TRIP,
    });
  }
  next();
};

export const checkStartAndEndDate = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  if (endDate < startDate) {
    return res.status(BAD_REQUEST).json({
      error: INVALID_DATE,
    });
  }
  next();
};

export const doDatesOverlap = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const busId = req.body.bus.id;
  const startDate = req.body.startDate;
  const endDate = req.body.endDate;

  const trip = await checkExistingTrip(busId, startDate, endDate);
  if (trip && trip.id === req.body.trip.id) {
    next();
  } else {
    return res.status(BAD_REQUEST).json({
      error: DUPLICATE_TRIP,
    });
  }
};
