import { Request, Response, NextFunction } from "express";
import { findTripByBookingId } from "../dbLayer/booking.db";
import { bookingValidationSchema } from "./validation.schema";
import {
  BOOKING_NOT_FOUND,
  CANCELLED_TICKET,
  CREATOR,
  INVALID_BOOKING_DATA,
} from "../utils/apiResponseMessages";
import {
  BAD_REQUEST,
  NOT_FOUND,
  ACCESS_DENIED,
} from "../utils/apiResponseStatusCode";

export const bookingRegistrationSchemaValidator = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = bookingValidationSchema.validate(req.body);
  if (error) {
    return res.status(BAD_REQUEST).json({
      error: error.details[0].message,
    });
  }
  const passangerNamesLength = req.body.passangerNames.length;
  const genderLength = req.body.gender.length;
  const ageLength = req.body.age.length;
  const noOfSeats = req.body.noOfSeats;
  if (
    ageLength !== noOfSeats ||
    genderLength !== noOfSeats ||
    passangerNamesLength !== noOfSeats
  ) {
    return res.status(BAD_REQUEST).json({
      error: INVALID_BOOKING_DATA,
    });
  }
  next();
};

export const getBookingById = async (
  req: Request,
  res: Response,
  next: NextFunction,
  id: string
) => {
  const trip = await findTripByBookingId(id);
  if (trip) {
    req.body.trip = trip;
    next();
  } else {
    return res.status(NOT_FOUND).json({
      error: BOOKING_NOT_FOUND,
    });
  }
};

export const isCreator = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userId = req.body.auth.userId;
  const operatorId = String(req.body.bus.operatorId);

  if (operatorId === userId) {
    return res.status(ACCESS_DENIED).json({
      error: CREATOR,
    });
  }
  next();
};

export const hasBooked = (req: Request, res: Response, next: NextFunction) => {
  const userId = req.body.auth.userId;

  const bookedUserId = String(req.body.trip.bookings.userId);

  if (userId === bookedUserId) {
    next();
  } else {
    return res.status(BAD_REQUEST).json({
      error: BOOKING_NOT_FOUND,
    });
  }
};

export const hasTripEnded = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const endDate = req.body.trip.endDate;
  const currentDate = new Date();

  if (endDate < currentDate) {
    return res.status(BAD_REQUEST).json({
      error: CANCELLED_TICKET,
    });
  }

  next();
};
