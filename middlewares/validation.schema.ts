import Extension from "@joi/date";
import Joi from "joi";

const joiDate = Joi.extend(Extension);

export const userRegistartionValidationSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string(),
  email: Joi.string().email({ minDomainSegments: 2 }).required(),
  password: Joi.string()
    .pattern(
      new RegExp(
        /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/
      )
    )
    .required()
    .messages({
      "string.pattern.base": `Password should have Atleast one speacial character , one number , one uppercase character and should be of length 6 to 16`,
    }),
  type: Joi.string().valid("customer", "operator", "admin").required(),
});

export const loginValidationSchema = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2 }).required(),
  password: Joi.string().required(),
});

export const busRegistrationValidationSchema = Joi.object({
  bus: Joi.object(),
  name: Joi.string().required(),
  totalSeats: Joi.number().greater(10).required(),
  registrationNumber: Joi.string().alphanum().required(),
  isAc: Joi.boolean().required(),
  isSleeper: Joi.boolean().required(),
});

const previousDate = new Date();
previousDate.setDate(previousDate.getDate() - 1);

export const tripRegistrationValidationSchema = Joi.object({
  bus: Joi.object(),
  trip: Joi.object(),
  totalSeats: Joi.number().greater(10).required(),
  from: Joi.string().required(),
  to: Joi.string().required(),
  startDate: joiDate
    .date()
    .format("YYYY-MM-DD")
    .iso()
    .greater(previousDate)
    .required(),
  endDate: joiDate
    .date()
    .format("YYYY-MM-DD")
    .iso()
    .greater(previousDate)
    .required(),
  sleeperPrice: Joi.number().required(),
  seaterPrice: Joi.number().required(),
  upperPrice: Joi.number().required(),
  totalSleeper: Joi.number().required(),
  totalSeater: Joi.number().required(),
  totalUpper: Joi.number().required(),
});

export const bookingValidationSchema = Joi.object({
  auth: Joi.object(),
  bus: Joi.object(),
  trip: Joi.object(),
  booking: Joi.object(),
  noOfSeats: Joi.number().greater(0).required(),
  seatNo: Joi.array().items(Joi.string()).required(),
  passangerNames: Joi.array().items(Joi.string()).required(),
  gender: Joi.array()
    .items(Joi.string().valid("male", "female", "other"))
    .required(),
  age: Joi.array().items(Joi.number()).required(),
});
