import { Schema, model } from "mongoose";
import { IBooking } from "../interfaces";

export const bookingSchema = new Schema<IBooking>({
  userId: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  noOfBookedSeats: {
    type: Number,
    required: true,
  },
  seatNo: {
    type: [
      {
        type: String,
      },
    ],
    required: true,
  },
  totalFair: {
    type: Number,
    required: true,
  },
  passangerNames: [
    {
      type: String,
    },
  ],
  gender: [
    {
      type: String,
    },
  ],
  age: [
    {
      type: Number,
    },
  ],
  bookedDate: {
    type: Date,
    default: Date.now,
  },
  travellingDate: {
    type: Date,
    required: true,
  },
});

export const Booking = model<IBooking>("booking", bookingSchema);
