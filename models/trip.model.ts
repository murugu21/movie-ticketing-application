import { bookingSchema } from "./booking.model";
import { seatSchema } from "./seat.model";
import { Schema, model } from "mongoose";
import { ITrip } from "../interfaces";

const tripSchema = new Schema<ITrip>({
  busId: {
    type: Schema.Types.ObjectId,
    ref: "bus",
    required: true,
  },
  operatorId: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  totalSeats: {
    type: Number,
    required: true,
  },
  seat: [
    {
      type: seatSchema,
    },
  ],
  availableSeats: {
    type: [String],
  },
  from: {
    type: String,
    required: true,
  },
  to: {
    type: String,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  isAc: {
    type: Boolean,
    required: true,
  },
  isSleeper: {
    type: Boolean,
    required: true,
  },
  sleeperPrice: {
    type: Number,
    required: true,
  },
  seaterPrice: {
    type: Number,
    required: true,
  },
  upperPrice: {
    type: Number,
    required: true,
  },
  totalSleeper: {
    type: Number,
    required: true,
  },
  totalSeater: {
    type: Number,
    required: true,
  },
  totalUpper: {
    type: Number,
    required: true,
  },
  bookings: [
    {
      type: bookingSchema,
    },
  ],
});

export const Trip = model<ITrip>("trip", tripSchema);
