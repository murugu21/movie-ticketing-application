import { Schema, model } from "mongoose";
import { IBus } from "../interfaces";

const busSchema = new Schema<IBus>({
  name: {
    type: String,
    required: true,
  },
  operatorId: {
    type: Schema.Types.ObjectId,
    ref: "user",
  },
  totalSeats: {
    type: Number,
    required: true,
  },
  registrationNumber: {
    type: String,
    required: true,
  },
  isAc: {
    type: Boolean,
    required: true,
  },
  isSleeper: {
    type: Boolean,
    required: true,
  },
});

export const Bus = model<IBus>("bus", busSchema);
