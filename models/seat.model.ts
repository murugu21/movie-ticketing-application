import mongoose from "mongoose";
import { ISeat } from "../interfaces";

export const seatSchema = new mongoose.Schema<ISeat>({
  seatNo: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ["sleeper", "upper", "seater"],
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
});
