import { ISeat } from "../interfaces";
/**
 * *Calculates total fair based on SeatNo
 * @param seatNo SeatNo entered by user
 * @param seat Object of type ISeat , which contains seat information
 * @returns Totalfair of ticket
 */
export const calculateFair = (seatNo: string[], seat: ISeat[]) => {
  let fair = 0;
  seat.map((seat) => {
    if (seatNo.includes(seat.seatNo)) {
      fair += seat.price;
    }
  });
  return fair;
};
