/**
 * *Returns start and enddate to check availablity of trip
 * @param dateOfTravel Date of Travel
 * @returns An object with start and enddate
 */
export const getStartDateEndDate = (dateOfTravel: string) => {
  const startDate = new Date(dateOfTravel);
  startDate.setHours(0, 0, 0, 0);

  const endDate = new Date(dateOfTravel);
  endDate.setDate(startDate.getDate() + 1);
  return { startDate, endDate };
};
