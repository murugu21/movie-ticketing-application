import { updateTrip, findTripBasedOnFilters } from "../dbLayer/trip.db";
import {
  ISeat,
  ITripFilters,
  ISeatsGenerationInfo,
  IUpdateInfo,
} from "../interfaces";
import { Trip } from "../models/trip.model";

/**
 * * Generate seats based on Information provided while creating trip
 * @param seatInfo Contains information to generate seats
 * @returns Returns an array of setas
 */
export const generateSeats = (seatInfo: ISeatsGenerationInfo) => {
  const {
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  } = seatInfo;

  const seats: ISeat[] = [];
  const isBooked = false;
  if (totalSeater === 0 && totalSleeper && totalUpper) {
    for (let i = 0; i < totalSleeper; i++) {
      seats.push({
        seatNo: `sle.${i}`,
        type: "sleeper",
        price: sleeperPrice,
        isBooked: isBooked,
      });
    }
    for (let i = 0; i < totalUpper; i++) {
      seats.push({
        seatNo: `upp.${i}`,
        type: "upper",
        price: upperPrice,
        isBooked: isBooked,
      });
    }
    return seats;
  }

  if (totalSleeper && totalUpper && totalSeater) {
    for (let i = 0; i < totalSleeper; i++) {
      seats.push({
        seatNo: `sle.${i}`,
        type: "sleeper",
        price: sleeperPrice,
        isBooked: isBooked,
      });
    }

    for (let i = 0; i < totalSeater; i++) {
      seats.push({
        seatNo: `sea.${i}`,
        type: "seater",
        price: seaterPrice,
        isBooked: isBooked,
      });
    }

    for (let i = 0; i < totalUpper; i++) {
      seats.push({
        seatNo: `upp.${i}`,
        type: "upper",
        price: upperPrice,
        isBooked: isBooked,
      });
    }
    return seats;
  }

  if (totalSeater && !totalUpper && !totalSleeper) {
    for (let i = 0; i < totalSeater; i++) {
      seats.push({
        seatNo: `sea.${i}`,
        type: "seater",
        price: seaterPrice,
        isBooked: isBooked,
      });
    }
    return seats;
  }
  return seats;
};

/**
 * * Updates new values in trip based on trip id
 * @param id Id of Trip document
 * @param newValues Values to update in trip document
 * @returns updated trip object
 */
export const updateTripWithNewValues = async (
  id: string,
  newValues: IUpdateInfo
) => {
  const {
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  } = newValues;
  const seats = generateSeats({
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  });
  const availableSeats = seats.map((seat) => seat.seatNo);
  const valuesToUpdate: IUpdateInfo = Object.assign({}, newValues, {
    seat: seats,
    availableSeats,
  });
  const tripId = id;
  const updatedTrip = await updateTrip(tripId, valuesToUpdate);
  return updatedTrip;
};
/**
 * * Fetches Trips from database based on given filters
 * @param filters Based on these filters trips are fetched from database
 * @returns Trip array that match the filters
 */
export const getTripsBasedOnFilters = async (filters: ITripFilters) => {
  const { startLocation, endLocation, startDate, endDate } = filters;
  let { isAc, isSleeper } = filters;

  let trips = null;

  if (!isAc && !isSleeper) {
    trips = await Trip.find({
      from: startLocation,
      to: endLocation,
      startDate,
      endDate,
    });
  } else if (!isAc && isSleeper) {
    isSleeper = isSleeper.toLowerCase();
    const isSleeperFlag = isSleeper === "true";
    trips = await Trip.find({
      from: startLocation,
      to: endLocation,
      startDate,
      endDate,
      isSleeper: isSleeperFlag,
    });
  } else if (isAc && !isSleeper) {
    isAc = isAc.toLowerCase();
    const isAcFlag = isAc === "true";
    trips = await Trip.find({
      from: startLocation,
      to: endLocation,
      startDate,
      endDate,
      isAc: isAcFlag,
    });
  } else {
    isSleeper = isSleeper.toLowerCase();
    const isSleeperFlag = isSleeper === "true";
    isAc = isAc.toLowerCase();
    const isAcFlag = isAc === "true";

    trips = await findTripBasedOnFilters({
      from: startLocation,
      to: endLocation,
      startDate,
      endDate,
      isSleeper: isSleeperFlag,
      isAc: isAcFlag,
    });
  }
  return trips;
};
