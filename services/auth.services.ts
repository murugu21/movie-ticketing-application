import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { SECRET_KEY } from "../config";
import { IUser } from "../interfaces";

/**
 ** compares password
 * @param passwordFromDB Password of a user fectec from Database
 * @param passwordEntered Actuall password entered by user
 * @returns isMatch A boolean value
 */
export const comparePassword = async (
  passwordFromDB: string,
  passwordEntered: string
) => {
  const isMatch = await bcrypt.compare(passwordFromDB, passwordEntered);
  return isMatch;
};
/**
 * *creates token based on user data
 * @param user User object requried to create JWT token
 * @returns token
 */
export const tokenize = (user: Partial<IUser> & { id: string }) => {
  const { id, email, type } = user;
  const token = jwt.sign({ userId: id, email, type }, SECRET_KEY, {
    expiresIn: "10d",
  });
  return token;
};
