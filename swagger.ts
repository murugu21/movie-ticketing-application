// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore: swagger autogen does not support typescript types
// eslint-disable-next-line @typescript-eslint/no-var-requires
const swaggerAutogen = require("swagger-autogen")({ openapi: "3.0.0" });

const doc = {
  info: {
    version: "1.0.0",
    title: "REDBUS API",
    description: "Documentation of API used in redbus ",
  },
  host: "redbus-backend-api.herokuapp.com",
  basePath: "/",
  schemes: ["https"],
  tags: [
    {
      name: "Auth",
      description: "Registration and login of users",
    },
    {
      name: "Bus",
      description: "Conitans APIs to create , update , delete and get buses",
    },
    {
      name: "Trip",
      description: "Conitans APIs to create , update , delete and get Trips",
    },
    {
      name: "Booking",
      description: "Conitans APIs to create , update , delete and get Bookings",
    },
    {
      name: "User",
      description: "Conitans APIs to get user details and  user bookings",
    },
  ],
  definitions: {
    RegisterUser: {
      firstName: "Harish",
      lastName: "Kumar",
      email: "hello.1@gamil.com",
      password: "Harish@07",
      type: "operator",
    },
    LoginUser: {
      email: "hello@gamil.com",
      password: "Harish@07",
    },
    RegisterBus: {
      name: "ABC",
      totalSeats: 30,
      registrationNumber: "TN20uU0021",
      isAc: true,
      isSleeper: true,
    },
    RegisterTrip: {
      totalSeats: 30,
      bookedSeats: 0,
      from: "coimbatore",
      to: "madurai",
      startDate: "2022-08-19T13:00:00.00Z",
      endDate: "2022-08-21T19:00:00.00Z",
      sleeperPrice: 1000,
      seaterPrice: 0,
      upperPrice: 900,
      totalSleeper: 15,
      totalSeater: 0,
      totalUpper: 15,
    },
    CreateBooking: {
      noOfSeats: 2,
      seatNo: ["upp.3", "upp.4"],
      seatType: "upper",
      passangerNames: ["harish", "kumar"],
      gender: ["male", "male"],
      age: [20, 21],
    },
    RegistartionLoginResponse: {
      message: "message",
      token: "Authorization token",
    },
    BusTripBookingResponse: {
      message: "message for successful creatiion",
      id: "Object ID ",
    },
    GetAllBusResponse: {
      buses: [
        {
          _id: "62fdcfb1c63f685484f8d8ec",
          name: "ABC",
          operatorId: "62fdcf92c63f685484f8d8e8",
          totalSeats: 30,
          registrationNumber: "TN20uU05021",
          isAc: true,
          isSleeper: true,
          __v: 0,
        },
      ],
    },
    GetBusResponse: {
      bus: {
        _id: "62fdef29cbb98146030332fc",
        name: "ABC",
        operatorId: "62fdcf92c63f685484f8d8e8",
        totalSeats: 30,
        registrationNumber: "TN20uU0021",
        isAc: true,
        isSleeper: true,
        __v: 0,
      },
    },
    GetTripResponse: {
      trip: {
        _id: "62fe0b57fa2cb3c0ab298115",
        busId: "62fdef29cbb98146030332fc",
        operatorId: "62fdcf92c63f685484f8d8e8",
        totalSeats: 30,
        bookedSeats: 0,
        from: "coimbatore",
        to: "madurai",
        startDate: "2022-08-23T13:00:00.000Z",
        endDate: "2022-08-24T19:00:00.000Z",
        isAc: true,
        isSleeper: true,
        sleeperPrice: 1000,
        seaterPrice: 0,
        upperPrice: 900,
        totalSleeper: 15,
        totalSeater: 0,
        totalUpper: 15,
        seat: [
          {
            seatNo: "sle.0",
            type: "sleeper",
            price: 1000,
            isBooked: false,
            _id: "62fe14a43628f8dd1abc4c9e",
          },
        ],
      },
    },
    GetTripsListResponse: {
      trips: [
        {
          _id: "62fe0b57fa2cb3c0ab298115",
          busId: "62fdef29cbb98146030332fc",
          operatorId: "62fdcf92c63f685484f8d8e8",
          totalSeats: 30,
          bookedSeats: 2,
          from: "coimbatore",
          to: "madurai",
          startDate: "2022-08-23T13:00:00.000Z",
          endDate: "2022-08-24T19:00:00.000Z",
          isAc: true,
          isSleeper: true,
          sleeperPrice: 1000,
          seaterPrice: 0,
          upperPrice: 900,
          totalSleeper: 15,
          totalSeater: 0,
          totalUpper: 15,
          seat: [
            {
              seatNo: "sle.0",
              type: "sleeper",
              price: 1000,
              isBooked: false,
              _id: "62fe14a43628f8dd1abc4c9e",
            },
          ],
        },
      ],
    },
    GetTripStatusResponse: {
      message: {
        isFull: false,
        totalSeats: 30,
        bookedSeats: 2,
        startDate: "2022-08-23T13:00:00.000Z",
      },
    },
    GetBookingResponse: {
      booking: {
        _id: "62fdd2f1fd4efd6f4ae3d9d5",
        busId: "62fdcfb1c63f685484f8d8ec",
        userId: "62fdd03ec63f685484f8d92f",
        tripId: "62fdd01cc63f685484f8d90e",
        name: "ABC",
        registrationNumber: "TN20uU05021",
        noOfBookedSeats: 2,
        seatNo: [],
        type: "upper",
        totalFair: 1800,
        passangerNames: [],
        gender: [],
        age: [],
        status: "booked",
        travellingDate: "2022-08-19T13:00:00.000Z",
        bookedDate: "2022-08-18T05:49:37.856Z",
        __v: 0,
      },
    },
    GetUserResponse: {
      user: {
        _id: "62fdd03ec63f685484f8d92f",
        firstName: "harish",
        lastName: "Kumar",
        email: "harish.1@gamil.com",
        type: "customer",
        bookings: [
          "62fdd2f1fd4efd6f4ae3d9d5",
          "62fdd304fd4efd6f4ae3d9f9",
          "62fdd8c0fd4311f8b3ff0956",
          "62fe36ca05dccd77cf904863",
        ],
        __v: 1,
      },
    },
    GetBookingListResponse: {
      bookings: [
        {
          _id: "62fdd2f1fd4efd6f4ae3d9d5",
          busId: "62fdcfb1c63f685484f8d8ec",
          userId: "62fdd03ec63f685484f8d92f",
          tripId: "62fdd01cc63f685484f8d90e",
          name: "ABC",
          registrationNumber: "TN20uU05021",
          noOfBookedSeats: 2,
          seatNo: [],
          type: "upper",
          totalFair: 1800,
          passangerNames: [],
          gender: [],
          age: [],
          status: "booked",
          travellingDate: "2022-08-19T13:00:00.000Z",
          bookedDate: "2022-08-18T05:49:37.856Z",
          __v: 0,
        },
      ],
    },
  },
};

const outputFile = "./dist/swagger_output.json";
const endpointsFiles = ["./server"];

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
  require("./index");
  process.exit();
});
