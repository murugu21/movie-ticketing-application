import dotenv from "dotenv";
dotenv.config();

export const PORT = process.env.PORT || "4000";
export const mongoURI = process.env.MONGO_URI;
export const SECRET_KEY = process.env.JWT_SECRET || "secret";
export const MONGODB_TEST_URL = process.env.MONGODB_TEST_URL;
