import { connect } from "mongoose";
import "dotenv/config";

export async function connectMainDB() {
  await connect(`${process.env.MONGODB_URL}`);
}
