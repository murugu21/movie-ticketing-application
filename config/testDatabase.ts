import mongoose from "mongoose";

import { MongoMemoryServer } from "mongodb-memory-server";
import { MONGODB_TEST_URL } from "../config";

let mongod: MongoMemoryServer | null = null;

export const connectTestDB = async () => {
  let dbUrl = MONGODB_TEST_URL;
  if (process.env.NODE_ENV === "test") {
    mongod = await MongoMemoryServer.create();
    dbUrl = mongod.getUri();
  }
  await mongoose.connect(`${dbUrl}`);
};

export const disconnectTestDB = async () => {
  await mongoose.connection.close();
  if (mongod) {
    await mongod.stop();
  }
};
