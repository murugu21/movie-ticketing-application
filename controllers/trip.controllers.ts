import { Request, Response } from "express";
import { getStartDateEndDate } from "../services/bus.services";
import {
  generateSeats,
  updateTripWithNewValues,
  getTripsBasedOnFilters,
} from "../services/trip.services";
import {
  createNewTrip,
  deleteTripById,
  findTripByOperatorId,
} from "../dbLayer/trip.db";
import {
  NO_TRIP,
  TRIP_CREATED,
  TRIP_DELETED,
  TRIP_NOT_FOUND,
  TRIP_UPDATED,
} from "../utils/apiResponseMessages";
import { CREATED, SUCCESS } from "../utils/apiResponseStatusCode";

export const createTrip = async (req: Request, res: Response) => {
  const busId = req.body.bus._id;
  const operatorId = req.body.auth.userId;
  const { isAc, isSleeper } = req.body.bus;
  const {
    totalSeats,
    bookedSeats,
    from,
    to,
    startDate,
    endDate,
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  } = req.body;

  const seats = generateSeats({
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  });
  const availableSeats = seats.map((seat) => seat.seatNo);
  const trip = {
    busId,
    operatorId,
    totalSeats,
    bookedSeats,
    seat: seats,
    availableSeats,
    from,
    to,
    startDate,
    endDate,
    isAc,
    isSleeper,
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  };
  const createdTrip = await createNewTrip(trip);
  return res.status(CREATED).json({
    message: TRIP_CREATED,
    id: createdTrip.id,
  });
};

export const getTrip = async (req: Request, res: Response) => {
  const trip = req.body.trip;
  return res.status(SUCCESS).json({ trip });
};

export const updateTrip = async (req: Request, res: Response) => {
  const trip = req.body.trip;
  const {
    totalSeats,
    bookedSeats,
    from,
    to,
    startDate,
    endDate,
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  } = req.body;

  updateTripWithNewValues(trip.id, {
    totalSeats,
    bookedSeats,
    from,
    to,
    startDate,
    endDate,
    sleeperPrice,
    seaterPrice,
    upperPrice,
    totalSleeper,
    totalSeater,
    totalUpper,
  });
  return res.status(SUCCESS).json({
    message: TRIP_UPDATED,
  });
};

export const deleteTrip = async (req: Request, res: Response) => {
  await deleteTripById(req.body.trip.id);

  return res.status(SUCCESS).json({
    message: TRIP_DELETED,
  });
};

export const getAllTrips = async (req: Request, res: Response) => {
  const startLocation = req.query.from as string;
  const endLocation = req.query.to as string;
  const dateOfTravel = req.query.onDate as string;
  const { startDate, endDate } = getStartDateEndDate(dateOfTravel);
  const isAc = req.query.requireAc ? (req.query.requireAc as string) : "";
  const isSleeper = req.query.requireSleeper
    ? (req.query.requireSleeper as string)
    : "";

  const trips = await getTripsBasedOnFilters({
    startLocation,
    endLocation,
    startDate,
    endDate,
    isAc,
    isSleeper,
  });
  if (trips.length === 0) {
    return res.status(SUCCESS).json({
      message: `${NO_TRIP} ${dateOfTravel},`,
    });
  }
  return res.status(SUCCESS).json({
    trips,
  });
};

export const getStatus = async (req: Request, res: Response) => {
  const { totalSeats, bookedSeats, startDate } = req.body.trip;
  const totalAvailableSeats = totalSeats - bookedSeats;
  return res.status(SUCCESS).json({
    message: {
      availableSeats: totalAvailableSeats,
      startDate: startDate,
    },
  });
};

export const getAllTripsRegisteredByOperator = async (
  req: Request,
  res: Response
) => {
  const userId = req.body.auth.userId;
  const trips = await findTripByOperatorId(userId);
  if (trips.length === 0) {
    return res.status(SUCCESS).json({ message: TRIP_NOT_FOUND });
  }
  return res.status(SUCCESS).json({
    trips,
  });
};
