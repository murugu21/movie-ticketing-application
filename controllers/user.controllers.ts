import { Request, Response } from "express";
import { findUserById } from "../dbLayer/auth.db";
import { findBookingsBasedOnUserId } from "../dbLayer/booking.db";
import { BOOKING_NOT_FOUND, NO_USER_FOUND } from "../utils/apiResponseMessages";
import { NOT_FOUND, SUCCESS } from "../utils/apiResponseStatusCode";

export const getUserById = async (req: Request, res: Response) => {
  const userId = req.body.auth.userId;
  const user = await findUserById(userId);
  if (!user) {
    return res.status(NOT_FOUND).json({
      error: NO_USER_FOUND,
    });
  }
  return res.status(SUCCESS).json({ user });
};

export const getBookingHistory = async (req: Request, res: Response) => {
  const userId = req.body.auth.userId;
  const bookings = await findBookingsBasedOnUserId(userId);
  if (bookings.length === 0) {
    return res.status(SUCCESS).json({
      message: BOOKING_NOT_FOUND,
    });
  }

  return res.status(SUCCESS).json({ bookings });
};
