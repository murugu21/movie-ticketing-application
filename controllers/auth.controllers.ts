import { Request, Response } from "express";
import bcrypt from "bcrypt";
import { comparePassword, tokenize } from "../services/auth.services";
import { createUser, findUserByMail } from "../dbLayer/auth.db";

import {
  USER_CREATED,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from "../utils/apiResponseMessages";
import { CREATED, SUCCESS } from "../utils/apiResponseStatusCode";

export const signUp = async (req: Request, res: Response) => {
  const { firstName, lastName, email, password, type } = req.body;
  const encrypted_password = await bcrypt.hash(password, 10);

  const user = await createUser({
    firstName,
    lastName,
    email: email.toLowerCase(),
    password: encrypted_password,
    type: type,
  });
  const id = user._id.toString();
  const token = tokenize({ id, email, type });
  return res.status(CREATED).json({
    message: USER_CREATED,
    token: token,
  });
};

export const signIn = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const user = await findUserByMail(email);
  if (user && (await comparePassword(password, user.password))) {
    const { id, email, type } = user;
    const token = tokenize({ id, email, type });
    res.status(SUCCESS).json({
      message: LOGIN_SUCCESS,
      token: token,
    });
  } else {
    res.status(403).json({
      error: LOGIN_FAILURE,
    });
  }
};
