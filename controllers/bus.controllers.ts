import { Request, Response } from "express";
import {
  createBusDB,
  findBusByOperatorId,
  updateBusById,
  deleteBusById,
} from "../dbLayer/bus.db";
import {
  BUS_CREATED,
  BUS_DELETED,
  BUS_NOT_FOUND,
  BUS_UPDATED,
} from "../utils/apiResponseMessages";
import { CREATED, SUCCESS } from "../utils/apiResponseStatusCode";

export const createBus = async (req: Request, res: Response) => {
  const bus = await createBusDB(req.body);
  return res.status(CREATED).json({
    message: BUS_CREATED,
    id: bus.id,
  });
};

export const getBus = async (req: Request, res: Response) => {
  const bus = req.body.bus;
  return res.status(SUCCESS).json({ bus });
};
export const getAllRegistertedBus = async (req: Request, res: Response) => {
  const userId = req.body.auth.userId;
  const buses = await findBusByOperatorId(userId);
  if (buses.length === 0) {
    return res.status(SUCCESS).json({ message: BUS_NOT_FOUND });
  }
  return res.status(SUCCESS).json({ buses });
};
export const updateBus = async (req: Request, res: Response) => {
  await updateBusById(req.body.bus.id, req.body);
  return res.status(SUCCESS).json({
    message: BUS_UPDATED,
  });
};

export const deleteBus = async (req: Request, res: Response) => {
  await deleteBusById(req.body.bus.id);
  return res.status(SUCCESS).json({
    message: BUS_DELETED,
  });
};
