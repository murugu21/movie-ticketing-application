import { Request, Response } from "express";
import { calculateFair } from "../services/booking.services";
import {
  updateTripOnBookingCreation,
  updateTripOnBookingUpdation,
  updateTripOnBookingDeletion,
} from "../dbLayer/trip.db";
import { Booking } from "../models/booking.model";
import {
  BOOKING_CREATED,
  BOOKING_DELETED,
  BOOKING_UPDATED,
  SEAT_NOT_AVAILABLE,
} from "../utils/apiResponseMessages";
import { BAD_REQUEST, CREATED, SUCCESS } from "../utils/apiResponseStatusCode";

export const createBooking = async (req: Request, res: Response) => {
  const { startDate, seat } = req.body.trip;
  const { userId } = req.body.auth;
  const tripId = req.body.trip.id;

  const { noOfSeats, passangerNames, gender, age, seatNo } = req.body;

  const totalFair = calculateFair(seatNo, seat);

  const createdBooking = new Booking({
    userId: userId,
    noOfBookedSeats: noOfSeats,
    seatNo: seatNo,
    totalFair: totalFair,
    passangerNames: passangerNames,
    gender: gender,
    age: age,
    travellingDate: startDate,
  });
  const updatedTrip = await updateTripOnBookingCreation(tripId, createdBooking);
  if (!updatedTrip) {
    return res.status(BAD_REQUEST).json({
      error: SEAT_NOT_AVAILABLE,
    });
  }
  return res.status(CREATED).json({
    message: BOOKING_CREATED,
    id: createdBooking.id,
  });
};

export const getBooking = (req: Request, res: Response) => {
  const booking = req.body.trip.bookings;
  res.status(SUCCESS).json({ booking });
};

export const updateBooking = async (req: Request, res: Response) => {
  const bookings = req.body.trip.bookings;
  const bookedSeatNo = req.body.trip.bookings.seatNo;
  const trip = req.body.trip;
  let { seatNo } = req.body;

  let availableSeats = trip.availableSeats;
  availableSeats = availableSeats.concat(bookedSeatNo);
  availableSeats = availableSeats.filter(
    (SeatNumber: string) => !seatNo.includes(SeatNumber)
  );
  seatNo = seatNo.filter((no: string) => !bookedSeatNo.includes(no));

  const updatedTrip = await updateTripOnBookingUpdation(
    bookings._id.toString(),
    availableSeats,
    req.body,
    seatNo
  );
  if (!updatedTrip) {
    return res.status(BAD_REQUEST).json({
      error: SEAT_NOT_AVAILABLE,
    });
  }
  return res.status(SUCCESS).json({
    message: BOOKING_UPDATED,
  });
};

export const deleteBooking = async (req: Request, res: Response) => {
  const bookingId = req.body.trip.bookings._id.toString();
  const bookedSeatNo = req.body.trip.bookings.seatNo;

  await updateTripOnBookingDeletion(bookingId, bookedSeatNo);
  return res.status(SUCCESS).json({
    message: BOOKING_DELETED,
  });
};
