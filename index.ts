import app from "./server";
import { connectMainDB } from "./config/database";
import { PORT } from "./config";

app.listen(PORT, () => {
  connectMainDB();
});
