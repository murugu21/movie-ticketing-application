import app from "../server";
import supertest from "supertest";
import { userOperator, userCustomer, busOperator } from "../seed/user.seed";
import { busForSeed, busForCreate, busForUpdate } from "../seed/bus.seed";
import { connectTestDB, disconnectTestDB } from "../config/testDatabase";
import {
  SUCCESS,
  CREATED,
  ACCESS_DENIED,
  INTERNAL_SERVER_ERROR,
} from "../utils/apiResponseStatusCode";
import {
  BUS_CREATED,
  DUPLICATE_BUS,
  NOT_OPERATOR,
  BUS_UPDATED,
  BUS_DELETED,
  INVAILD_ACCESS_OF_BUS,
} from "../utils/apiResponseMessages";
describe("All tests related to bus", () => {
  let operatorToken: string,
    customerToken: string,
    busId: string,
    operatorWithNoBusToken: string;
  beforeAll(async () => {
    await connectTestDB();

    const operator = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userOperator);

    const operatorWithNoBus = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(busOperator);

    const customer = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userCustomer);

    operatorToken = operator.body.token;
    customerToken = customer.body.token;
    operatorWithNoBusToken = operatorWithNoBus.body.token;

    const seedBus = await supertest(app)
      .post("/api/bus/")
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(busForSeed);
    busId = seedBus.body.id;
  });
  afterAll(async () => {
    await disconnectTestDB();
  });
  describe("POST /api/bus/", () => {
    describe("given name , totalSeats ,registrationNumber ,isAc , isSleeper", () => {
      test("should respond with CREATED status code and message", async () => {
        const res = await supertest(app)
          .post("/api/bus/")
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(busForCreate);
        expect(res.statusCode).toBe(CREATED);
        expect(res.body.message).toEqual(BUS_CREATED);
      });
    });
    describe("given name , totalSeats ,EXISTING - registrationNumber ,isAc , isSleeper", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post("/api/bus/")
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(busForCreate);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(DUPLICATE_BUS);
      });
    });
    describe("given name , totalSeats ,registrationNumber ,isAc , isSleeper with customer token", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post("/api/bus/")
          .set("Content-type", "application/json")
          .set("Authorization", customerToken)
          .send(busForCreate);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toBeDefined();
      });
    });
  });

  describe("GET /api/bus/all/", () => {
    describe("given operator auth token return all registered buses", () => {
      test("should respond with SUCCESS status code and buses", async () => {
        const res = await supertest(app)
          .get("/api/bus/all")
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.buses).toBeDefined();
      });
    });
    describe("given operator auth token that has  no registered buses", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get("/api/bus/all")
          .set("Content-type", "application/json")
          .set("Authorization", operatorWithNoBusToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toBeDefined();
      });
    });
    describe(" given invalid operator auth token ", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .get("/api/bus/all")
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(NOT_OPERATOR);
      });
    });
  });

  describe("GET /api/bus/:busId", () => {
    describe("given bus id return bus", () => {
      test("should respond with SUCCESS status code and bus", async () => {
        const res = await supertest(app)
          .get(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.bus).toBeDefined();
      });
    });
    describe("for a given bus id , return error incase of invalid authorization", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .get(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(NOT_OPERATOR);
      });
    });
    describe("Return error if busId is invalid", () => {
      test("should respond with NOT_FOUND status code and error", async () => {
        const res = await supertest(app)
          .get(`/api/bus/h${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toBeDefined();
      });
    });
  });

  describe("PUT  /api/bus/:busId", () => {
    describe("given new name , totalSeats ,registrationNumber ,isAc , isSleeper with busId", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .put(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(busForUpdate);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(BUS_UPDATED);
      });
    });
    describe("given new name , totalSeats ,registrationNumber ,isAc , isSleeper - with different authorization token", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .put(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorWithNoBusToken)
          .send(busForUpdate);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(INVAILD_ACCESS_OF_BUS);
      });
    });
  });

  describe("DELETE  /api/bus/:busId", () => {
    describe("given authorization token is wrong", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .delete(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorWithNoBusToken);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(INVAILD_ACCESS_OF_BUS);
      });
    });
    describe("given busId is wrong ", () => {
      test("should respond with NOT_FOUND status code and error", async () => {
        const res = await supertest(app)
          .delete(`/api/bus/${busId}34`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);

        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toBeDefined();
      });
    });
    describe("given correct authorization token ", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .delete(`/api/bus/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(BUS_DELETED);
      });
    });
  });
});
