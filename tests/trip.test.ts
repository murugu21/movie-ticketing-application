import app from "../server";
import supertest from "supertest";
import { connectTestDB, disconnectTestDB } from "../config/testDatabase";
import {
  seedForTrip,
  createTrip,
  invalidCreateTrip,
  overlapSeedTrip,
  seatBookedTrip,
  updateTrip,
  createTripForOverlap,
} from "../seed/trip.seed";
import {
  userOperator,
  createUser,
  busOperator,
  userCustomer,
} from "../seed/user.seed";
import { busForSeed } from "../seed/bus.seed";
import {
  SUCCESS,
  CREATED,
  ACCESS_DENIED,
  BAD_REQUEST,
  INTERNAL_SERVER_ERROR,
} from "../utils/apiResponseStatusCode";
import {
  BOOKED_TRIP,
  DATES_OVERLAP,
  DUPLICATE_TRIP,
  INVAILD_ACCESS_OF_BUS,
  TRIP_CREATED,
  TRIP_NOT_FOUND,
  TRIP_UPDATED,
} from "../utils/apiResponseMessages";
import { seedForBooking } from "../seed/booking.seed";
describe("All test related to TRIP", () => {
  let operatorToken: string,
    busId: string,
    invalidOperatorToken: string,
    tripId: string,
    bookedTripId: string,
    operatorWithNoTripsToken: string,
    operatorWithNoBusToken: string,
    customerToken: string,
    bookingId: string;
  beforeAll(async () => {
    await connectTestDB();
    const operator = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userOperator);
    const operatorWithNoBus = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(busOperator);
    operatorWithNoBusToken = operatorWithNoBus.body.token;
    operatorToken = operator.body.token;
    invalidOperatorToken = `${operatorToken}+1`;
    const operatorWithNoTrips = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(createUser);
    operatorWithNoTripsToken = operatorWithNoTrips.body.token;
    const seedBus = await supertest(app)
      .post("/api/bus/")
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(busForSeed);
    busId = seedBus.body.id;
    const seedTrip = await supertest(app)
      .post(`/api/trip/${busId}`)
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(seedForTrip);
    tripId = seedTrip.body.id;

    const bookedTrip = await supertest(app)
      .post(`/api/trip/${busId}`)
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(seatBookedTrip);
    bookedTripId = bookedTrip.body.id;

    const customer = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userCustomer);
    customerToken = customer.body.token;

    const booking = await supertest(app)
      .post(`/api/booking/${busId}/${bookedTripId}`)
      .set("Content-type", "application/json")
      .set("Authorization", customerToken)
      .send(seedForBooking);
    bookingId = booking.body.id;
  });

  afterAll(async () => {
    await disconnectTestDB();
  });

  describe("POST /api/trip/:busId", () => {
    describe("given all the required feild", () => {
      test("should respond with CREATED status code and message", async () => {
        const res = await supertest(app)
          .post(`/api/trip/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(createTrip);

        expect(res.statusCode).toBe(CREATED);
        expect(res.body.message).toEqual(TRIP_CREATED);
      });
    });
    describe("given wrong operator id", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/trip/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorWithNoBusToken)
          .send(createTrip);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(INVAILD_ACCESS_OF_BUS);
      });
    });
    describe("requreid feilds are not sent or sent in invalid format", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/trip/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(invalidCreateTrip);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(expect.any(String));
      });
    });

    describe("Given dates that overlaps with existing trip", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/trip/${busId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(createTripForOverlap);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(DATES_OVERLAP);
      });
    });
  });
  describe("PUT /api/trip/:busId/:tripId", () => {
    describe("given seats booked tripId , cannot update and return with BAD_REQUEST statuscode and error", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .put(`/api/trip/${busId}/${bookedTripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(updateTrip);

        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(BOOKED_TRIP);
      });
    });
    describe("given dates that overlaps with an  existing trip ", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .put(`/api/trip/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(overlapSeedTrip);

        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(DUPLICATE_TRIP);
      });
    });
    describe("given all the required feild , with no booked seats and overlaps on dates  ", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .put(`/api/trip/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken)
          .send(updateTrip);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(TRIP_UPDATED);
      });
    });
  });

  describe("GET /api/trip/all", () => {
    describe("Display all trips based on query, return SUCCESS status code and message", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get(
            "/api/trip/all?from=coimbatore&to=madurai&onDate=2022-08-25T13:22:48.705Z&requireAc=true&requireSleeper=true"
          )
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.trips).toBeDefined();
      });
    });
    describe("Display message if no trip was found, return SUCCESS status code and message", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get(
            "/api/trip/all?from=pune&to=madurai&onDate=2022-08-22T13:22:48.705Z&requireAc=true&requireSleeper=true"
          )
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(expect.any(String));
      });
    });
  });

  describe("GET /api/trip/operator/all/", () => {
    describe("Display all trips based on Auth Token, return SUCCESS status code and trips", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get("/api/trip/operator/alltrips/")
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.trips).toBeDefined();
      });
    });

    describe("Display all trips based on Auth Token, if no trips are  registered return SUCCESS status code and message", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get("/api/trip/operator/alltrips/")
          .set("Content-type", "application/json")
          .set("Authorization", operatorWithNoTripsToken);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(TRIP_NOT_FOUND);
      });
    });
  });
  describe("GET /api/trip/:tripId", () => {
    describe("Display the trip, return SUCCESS status code and trip", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get(`/api/trip/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.trip).toBeDefined();
      });
    });
    describe("Display  error if the Trip cannot be found based on tripId, return NOT_FOUND status code and error", () => {
      test("should respond with NOT_FOUND status code and error", async () => {
        const res = await supertest(app)
          .get(`/api/trip/${tripId}2`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toEqual(expect.any(String));
      });
    });
  });
  describe("GET /api/trip/status/:tripId", () => {
    describe("Display the trip status, return SUCCESS status code and message", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .get(`/api/trip/status/${tripId}`)
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toBeDefined();
      });
    });
    describe("Display  error if the Trip cannot be found based on invalid tripId, return INTERNAL_SERVER_ERROR status code and error", () => {
      test("should respond with NOT_FOUND status code and error", async () => {
        const res = await supertest(app)
          .get(`/api/trip/status/${tripId}5`)
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toEqual(expect.any(String));
      });
    });
  });

  describe("DELETE /api/trip/:busId/:tripId", () => {
    describe("given correct authorization token  and  seats are booked  and trip hasn't ended,  respond with appropriate status code and error", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .delete(`/api/trip/${busId}/${bookedTripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", operatorToken);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(BOOKED_TRIP);
      });
    });
  });
  describe("given incorrect authorization token , return ACCESS_DENIED status code and error ", () => {
    test("should respond with ACCESS_DENIED status code and error", async () => {
      const res = await supertest(app)
        .delete(`/api/trip/${busId}/${bookedTripId}`)
        .set("Content-type", "application/json")
        .set("Authorization", invalidOperatorToken);
      expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
      expect(res.body.error).toEqual(expect.any(String));
    });
  });
});
