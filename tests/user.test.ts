import app from "../server";
import supertest from "supertest";
import { connectTestDB, disconnectTestDB } from "../config/testDatabase";
import { userCustomer, userOperator, createCustomer } from "../seed/user.seed";
import { busForSeed } from "../seed/bus.seed";
import { seedForTrip } from "../seed/trip.seed";
import { seedForBooking } from "../seed/booking.seed";
import { SUCCESS, BAD_REQUEST } from "../utils/apiResponseStatusCode";
import {
  BOOKING_NOT_FOUND,
  NO_TOKEN_FOUND,
} from "../utils/apiResponseMessages";
describe("All test related to user", () => {
  let customerToken: string,
    operatorToken: string,
    customerWithNoBookingsToken: string,
    busId: string,
    tripId: string;

  beforeAll(async () => {
    await connectTestDB();
    const customer = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userCustomer);
    const operator = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userOperator);
    operatorToken = operator.body.token;
    customerToken = customer.body.token;
    const customerWithNoBookings = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(createCustomer);
    customerWithNoBookingsToken = customerWithNoBookings.body.token;
    const seedBus = await supertest(app)
      .post("/api/bus/")
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(busForSeed);
    busId = seedBus.body.id;
    const seedTrip = await supertest(app)
      .post(`/api/trip/${busId}`)
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(seedForTrip);
    tripId = seedTrip.body.id;
    await supertest(app)
      .post(`/api/booking/${busId}/${tripId}`)
      .set("Content-type", "application/json")
      .set("Authorization", customerToken)
      .send(seedForBooking);
  });

  afterAll(async () => {
    await disconnectTestDB();
  });

  describe("GET /api/user/", () => {
    describe("Display the user information based on token", () => {
      test("return SUCCESS statuscode with user", async () => {
        const res = await supertest(app)
          .get("/api/user/")
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.user).toBeDefined();
      });
    });

    describe("If the user token is absent , return error", () => {
      test("return BAD_REQUEST statuscode with error", async () => {
        const res = await supertest(app)
          .get("/api/user/")
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(NO_TOKEN_FOUND);
      });
    });
  });

  describe("GET /api/user/bookings", () => {
    describe("Display the user booking information", () => {
      test("return SUCCESS statuscode with bookings", async () => {
        const res = await supertest(app)
          .get("/api/user/bookings")
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.bookings).toBeDefined();
      });
    });

    describe("Display appropriate message if customer has no booking info", () => {
      test("return SUCCESS statuscode with message", async () => {
        const res = await supertest(app)
          .get("/api/user/bookings")
          .set("Content-type", "application/json")
          .set("Authorization", customerWithNoBookingsToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(BOOKING_NOT_FOUND);
      });
    });

    describe("If the user token is absent , return error", () => {
      test("return BAD_REQUEST statuscode with error", async () => {
        const res = await supertest(app)
          .get("/api/user/bookings")
          .set("Content-type", "application/json");
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(NO_TOKEN_FOUND);
      });
    });
  });
});
