import app from "../server";
import supertest from "supertest";
import {
  userOperator,
  createUser,
  createInvalidUser,
  inValidLogin,
  validLogin,
} from "../seed/user.seed";
import { connectTestDB, disconnectTestDB } from "../config/testDatabase";
import {
  SUCCESS,
  CREATED,
  ACCESS_DENIED,
  BAD_REQUEST,
} from "../utils/apiResponseStatusCode";
import { DUPLICATE_USER, LOGIN_FAILURE } from "../utils/apiResponseMessages";

describe(" All tests related to user/auth", () => {
  beforeAll(async () => {
    await connectTestDB();
    await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userOperator);
  });

  afterAll(async () => {
    await disconnectTestDB();
  });
  describe("POST /api/user/signin", () => {
    describe("given firstname , lastname ,email ,password , type", () => {
      test("should respond with SUCCESS status code and token", async () => {
        const res = await supertest(app)
          .post("/api/auth/signup")
          .set("Content-type", "application/json")
          .send(createUser);
        expect(res.statusCode).toBe(CREATED);
        expect(res.body.token).toEqual(expect.any(String));
      });
    });

    describe(" if  firstname , lastname ,email ,password , type is not a string or missing ", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .post("/api/auth/signup")
          .set("Content-type", "application/json")
          .send(createInvalidUser);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toBeDefined();
      });
    });

    describe("given firstname , lastname ,password , type,with existing email", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post("/api/auth/signup")
          .set("Content-type", "application/json")
          .send(userOperator);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(DUPLICATE_USER);
      });
    });
  });

  describe("POST /api/user/signup", () => {
    describe("given correct email and password", () => {
      test("should respond with SUCCESS status code and token", async () => {
        const res = await supertest(app)
          .post("/api/auth/signin")
          .set("Content-type", "application/json")
          .send(validLogin);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.token).toEqual(expect.any(String));
      });
    });

    describe(" if  email /password is/are incorrect or not specified ", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post("/api/auth/signin")
          .set("Content-type", "application/json")
          .send(inValidLogin);
        expect(res.statusCode).toBe(ACCESS_DENIED);
        expect(res.body.error).toEqual(LOGIN_FAILURE);
      });
    });
  });
});
