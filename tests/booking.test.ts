import {
  CREATED,
  INTERNAL_SERVER_ERROR,
} from "./../utils/apiResponseStatusCode";
import app from "../server";
import supertest from "supertest";
import { connectTestDB, disconnectTestDB } from "../config/testDatabase";
import { userOperator, userCustomer, createCustomer } from "../seed/user.seed";
import { busForSeed } from "../seed/bus.seed";
import { seedForTrip } from "../seed/trip.seed";
import {
  seedForBooking,
  createBooking,
  inValidCreateBooking,
  updateBooking,
} from "../seed/booking.seed";
import { SUCCESS, BAD_REQUEST } from "../utils/apiResponseStatusCode";
import {
  BOOKING_CREATED,
  BOOKING_DELETED,
  BOOKING_NOT_FOUND,
  BOOKING_UPDATED,
  INVALID_BOOKING_DATA,
  SEAT_NOT_AVAILABLE,
} from "../utils/apiResponseMessages";
describe("All tests related to booking", () => {
  let operatorToken: string,
    customerToken: string,
    busId: string,
    tripId: string,
    bookingId: string,
    customerWithNoBookingsToken: string;
  beforeAll(async () => {
    await connectTestDB();
    const operator = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userOperator);
    operatorToken = operator.body.token;
    const customer = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(userCustomer);
    customerToken = customer.body.token;
    const customerWithNoBookings = await supertest(app)
      .post("/api/auth/signup")
      .set("Content-type", "application/json")
      .send(createCustomer);
    customerWithNoBookingsToken = customerWithNoBookings.body.token;
    const seedBus = await supertest(app)
      .post("/api/bus/")
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(busForSeed);
    busId = seedBus.body.id;
    const seedTrip = await supertest(app)
      .post(`/api/trip/${busId}`)
      .set("Content-type", "application/json")
      .set("Authorization", operatorToken)
      .send(seedForTrip);
    tripId = seedTrip.body.id;
    const booking = await supertest(app)
      .post(`/api/booking/${busId}/${tripId}`)
      .set("Content-type", "application/json")
      .set("Authorization", customerToken)
      .send(seedForBooking);
    bookingId = booking.body.id;
  });

  afterAll(async () => {
    await disconnectTestDB();
  });

  describe("POST /api/booking/:busId/:tripId", () => {
    describe("given all the required feilds , should respond CREATED with message", () => {
      test("should respond with CREATED status code and message", async () => {
        const res = await supertest(app)
          .post(`/api/booking/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken)
          .send(createBooking);
        expect(res.statusCode).toBe(CREATED);
        expect(res.body.message).toEqual(BOOKING_CREATED);
      });
    });
    describe("given all the required feilds , with already booked seat no , should respond BAD_REQUEST statuscode with error", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/booking/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken)
          .send(seedForBooking);

        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(SEAT_NOT_AVAILABLE);
      });
    });
    describe("given all the required feilds , with invalid/insufficient passanger information , should respond BAD_REQUEST statuscode with error", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/booking/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken)
          .send(inValidCreateBooking);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(INVALID_BOOKING_DATA);
      });
    });
    describe("given all the required feilds , with no / invalid authorization token , should respond ACCESS_DENIED statuscode with error", () => {
      test("should respond with ACCESS_DENIED status code and error", async () => {
        const res = await supertest(app)
          .post(`/api/booking/${busId}/${tripId}`)
          .set("Content-type", "application/json")
          .set("Authorization", `${customerToken}0`)
          .send(createBooking);
        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toEqual(expect.any(String));
      });
    });
  });

  describe("PUT /api/booking/:bookingId/", () => {
    describe("If no issues with seat and dates , return with SUCCESS statuscode and message", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .put(`/api/booking/${bookingId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken)
          .send(updateBooking);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(BOOKING_UPDATED);
      });
    });
    describe("Given invalid Authorization token", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .put(`/api/booking/${bookingId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerWithNoBookingsToken)
          .send(updateBooking);

        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(BOOKING_NOT_FOUND);
      });
    });
  });
  describe("GET /api/:bookingId/", () => {
    describe("Display  booked information, return SUCCESS status code and booking", () => {
      test("should respond with SUCCESS status code and booking", async () => {
        const res = await supertest(app)
          .get(`/api/booking/${bookingId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);

        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.booking).toBeDefined();
      });
    });
    describe(" Return proper error message if wrong booking id is given", () => {
      test("should respond with INTERNAL_SERVER_ERROR  status code and error", async () => {
        const res = await supertest(app)
          .get(`/api/booking/${bookingId}s`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);

        expect(res.statusCode).toBe(INTERNAL_SERVER_ERROR);
        expect(res.body.error).toEqual(expect.any(String));
      });
    });
  });

  describe("DELETE /api/booking/:bookingId", () => {
    describe("if authorization token is incorrect , return BAD_REQUEST with error ", () => {
      test("should respond with BAD_REQUEST status code and error", async () => {
        const res = await supertest(app)
          .delete(`/api/booking/${bookingId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerWithNoBookingsToken);
        expect(res.statusCode).toBe(BAD_REQUEST);
        expect(res.body.error).toEqual(BOOKING_NOT_FOUND);
      });
    });

    describe("if authorization token is correct , delete the booking", () => {
      test("should respond with SUCCESS status code and message", async () => {
        const res = await supertest(app)
          .delete(`/api/booking/${bookingId}`)
          .set("Content-type", "application/json")
          .set("Authorization", customerToken);
        expect(res.statusCode).toBe(SUCCESS);
        expect(res.body.message).toEqual(BOOKING_DELETED);
      });
    });
  });
});
