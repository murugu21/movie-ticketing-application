export const busForSeed = {
  name: "ABC",
  totalSeats: 30,
  registrationNumber: "TN20uU0021",
  isAc: true,
  isSleeper: true,
};
export const busForCreate = {
  name: "ABC",
  totalSeats: 30,
  registrationNumber: "TN20uU021",
  isAc: true,
  isSleeper: true,
};
export const busForUpdate = {
  name: "ABC-updated",
  totalSeats: 30,
  registrationNumber: "TN20uU021",
  isAc: true,
  isSleeper: true,
};
