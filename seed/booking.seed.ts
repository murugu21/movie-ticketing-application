export const seedForBooking = {
  noOfSeats: 2,
  seatNo: ["upp.3", "upp.4"],
  passangerNames: ["harish", "kumar"],
  gender: ["male", "male"],
  age: [20, 21],
};
export const createBooking = {
  noOfSeats: 2,
  seatNo: ["upp.5", "upp.6"],
  passangerNames: ["harish", "kumar"],
  gender: ["male", "male"],
  age: [20, 21],
};

export const inValidCreateBooking = {
  noOfSeats: 2,
  seatNo: ["upp.5", "upp.6"],
  passangerNames: ["harish", "kumar"],
  gender: ["male", "male"],
  age: [20],
};

export const updateBooking = {
  noOfSeats: 1,
  seatNo: ["sle.1"],
  passangerNames: ["harish"],
  gender: ["male"],
  age: [20],
};
export const invalidUpdateBooking = {
  noOfSeats: 3,
  seatNo: ["upp.3", "upp.4"],
  passangerNames: ["harish", "kumar", "hk"],
  gender: ["male", "male", "male"],
  age: [20, 21, 20],
};
