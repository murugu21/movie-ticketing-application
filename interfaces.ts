import { Types } from "mongoose";

export interface IUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  type: string;
}
export interface IBus {
  name: string;
  operatorId: Types.ObjectId;
  totalSeats: number;
  registrationNumber: string;
  isAc: boolean;
  isSleeper: boolean;
}
export interface IBooking {
  userId: Types.ObjectId;
  noOfBookedSeats: number;
  seatNo: string[];
  totalFair: number;
  passangerNames: string[];
  gender: string[];
  age: number[];
  bookedDate?: Date;
  travellingDate: Date;
}

export interface ISeat {
  seatNo: string;
  type: string;
  price: number;
  isBooked: boolean;
}

export interface ITrip {
  busId: Types.ObjectId;
  operatorId: Types.ObjectId;
  totalSeats: number;
  seat: ISeat[];
  availableSeats: string[];
  from: string;
  to: string;
  startDate: Date;
  endDate: Date;
  isAc: boolean;
  isSleeper: boolean;
  sleeperPrice: number;
  seaterPrice: number;
  upperPrice: number;
  totalSleeper: number;
  totalSeater: number;
  totalUpper: number;
  bookings: IBooking[];
}
export interface IBookedSeats {
  noOfSeats: number;
}

export interface IUpdateInfo {
  totalSeats: number;
  bookedSeats: number;
  from: string;
  to: string;
  startDate: Date;
  endDate: Date;
  sleeperPrice: number;
  seaterPrice: number;
  upperPrice: number;
  totalSleeper: number;
  totalSeater: number;
  totalUpper: number;
  seat?: ISeat[];
}

export interface ISeatsGenerationInfo {
  sleeperPrice: number;
  seaterPrice: number;
  upperPrice: number;
  totalSleeper: number;
  totalSeater: number;
  totalUpper: number;
}

export interface ITripFilters {
  startLocation: string;
  endLocation: string;
  startDate: Date;
  endDate: Date;
  isAc: string;
  isSleeper: string;
}
