module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testPathIgnorePatterns: [".js"],
  modulePathIgnorePatterns: ["common"],
  collectCoverageFrom: ["**/*.ts", "!**/node_modules/**"],
  coverageReporters: ["html", "text", "text-summary", "cobertura"],
  testTimeout: 600000,
};
