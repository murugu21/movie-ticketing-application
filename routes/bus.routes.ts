import express from "express";
const router = express.Router();
import { wrapAsync, wrapAsyncMiddleware } from "../utils/asyncWrap.util";

import {
  isSignedin,
  isOperator,
  isAuthenticatedToAccessBus,
} from "../middlewares/auth.middlewares";
import {
  createBus,
  updateBus,
  deleteBus,
  getBus,
  getAllRegistertedBus,
} from "../controllers/bus.controllers";
import {
  getBusById,
  busRegistrationSchemaValidator,
  isBusExists,
  appendOperatorIdToBody,
} from "../middlewares/bus.middlewares";

router.param("busId", wrapAsyncMiddleware(getBusById));

/**
@route    POST api/bus/
@desc     Create new Bus
@access   Public
*/

router.post(
  "/",
  busRegistrationSchemaValidator,
  wrapAsync(isSignedin),
  isOperator,
  wrapAsync(isBusExists),
  appendOperatorIdToBody,
  wrapAsync(createBus)

  // #swagger.tags = ['Bus']
  // #swagger.summary = 'Returns message  on successful registration'
  // #swagger.description = 'Creates a new Bus document based on given data'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/RegisterBus" }
                  }
              }
          }
    */

  /*  #swagger.responses[201] = { 
      description: 'Bus created successfully!' ,
      schema: { $ref: "#/definitions/BusTripBookingResponse" }
    } */
  /*  #swagger.responses[400] = { 
      description: 'Invalid details entered, cannot create bus' 
  } */

  /* #swagger.responses[400] = { 
      description: 'Authorization Error'
  }  */

  /*  #swagger.responses[403] = { 
      description: 'Customers don't have permission to create bus' 
  } */

  /* #swagger.responses[403] = { 
      description: 'A bus with this registration number already exists' 
  } */
);
/**
@route    GET api/bus/all
@desc     'Returns all buses registered by operator'
@access   Public
*/
router.get(
  "/all",
  wrapAsync(isSignedin),
  isOperator,
  wrapAsync(getAllRegistertedBus)
  // #swagger.tags = ['Bus']
  // #swagger.summary = 'Returns all buses registered by operator'
  // #swagger.description = 'Returns all buses registered by operator only if the authorization token and bus id are valid'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

  /* #swagger.responses[200] = { 
      description: 'Returns all buses created by operator!',
    schema: { $ref: "#/definitions/GetAllBusResponse" }
    } */

  /* #swagger.responses[400] = { 
      description: 'Authorization Error'
  } */

  /* /#swagger.responses[403] = {
      description: 'Customers don't have permission to create bus' 
  }
 */
);
/**
@route    GET api/bus/busId
@desc     Returns bus based on Bus ID 
@access   Public
*/
router.get(
  "/:busId",
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  getBus

  // #swagger.tags = ['Bus']
  // #swagger.summary = 'Returns bus based on Bus ID '
  // #swagger.description = 'Returns bus requested only if the authorization token and bus id are valid '
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

  /* #swagger.responses[200] =  {
      description: 'Returns requested bus based on id!',
    schema: { $ref: "#/definitions/GetBusResponse" }
  } */

  /*  #swagger.res
    ponses[400] = {
      description: 'Authorization Error'
  } */

  /*  #swagger.responses[403] = {
      description: 'Customers don't have permission to create bus'
  } */

  /* /#swagger.responses[403] = {
      description: 'Not authorizred to access or change bus details'
  }  */
);

/**
@route    PUT api/bus/busId
@desc     Updates bus details
@access   Public
*/
router.put(
  "/:busId/",
  busRegistrationSchemaValidator,
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  wrapAsync(updateBus)

  // #swagger.tags = ['Bus']
  // #swagger.summary = 'Returns message on successful update '
  // #swagger.description = 'Updates the bus with new values only if the authorization token and bus id are valid '

  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/RegisterBus" }
                  }
              }
          }
    */
  /*  #swagger.responses[200] = { 
      description: 'Bus successfully updated' ,
    schema: { $ref: "#/definitions/BusTripBookingResponse" }
  } */
  /*  #swagger.responses[400] = {
       description: 'Invalid details entered, cannot create bus' 
  } */
  /* #swagger.responses[400] = {
        description: 'Authorization Error'
    } */

  /* #swagger.responses[403] = {
      description: 'Customers don't have permission to create bus'
  } */

  /*  #swagger.responses[403] = {
      description: 'Not authorizred to access or change bus details'
  }  */
);
/**
@route    DELETE api/bus/busId
@desc     deletes bus details
@access   Public
*/
router.delete(
  "/:busId/",
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  wrapAsync(deleteBus)
  // #swagger.tags = ['Bus']
  // #swagger.summary = 'Returns message on successful deletion '
  // #swagger.description = 'Deletes the bus  only if the authorization token and bus id are valid '

  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

  /*  #swagger.responses[200] = { 
      description: 'Bus successfully deleted' ,
    schema: { $ref: "#/definitions/BusTripBookingResponse" }
  } */
  /* /#swagger.responses[400] = {
   description: 'Authorization Error'
} */
  /* 
     #swagger.responses[403] = {
      description: 'Customers don't have permission to create bus'
  } */

  /*  #swagger.responses[403] = {
      description: 'Not authorizred to access or change bus details'
  } */
);

export default router;
