import { Router } from "express";
const router = Router();
import { signIn, signUp } from "../controllers/auth.controllers";
import {
  registartionSchemaValidator,
  loginSchemaValidator,
  isExistingUser,
} from "../middlewares/auth.middlewares";
import { wrapAsync } from "../utils/asyncWrap.util";

/**
@route    POST api/auth/signup
@desc     Register user & issue token
@access   Public
*/

router.post(
  "/signup",
  registartionSchemaValidator,
  wrapAsync(isExistingUser),
  wrapAsync(signUp)

  // #swagger.tags = ['Auth']
  // #swagger.summary = 'Returns token if registration is successful'
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/RegisterUser" }
                  }
              }
          }
  */

  // #swagger.responses[201] = { description: 'User registered successfully!' }

  // #swagger.responses[400] = { description: 'Invalid information' }

  // #swagger.responses[403] = { description: 'User with email already exists' ,schema: { $ref: "#/definitions/ErrorMessage" }}
);

/**
@route    POST api/auth/signin
@desc     Authenticate user & issue token
@access   Public
*/

router.post(
  "/signin",
  loginSchemaValidator,
  wrapAsync(signIn)

  // #swagger.tags = ['Auth']
  // #swagger.summary = 'Returns token for valid users'
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/LoginUser" }
                  }
              }
          }
    */

  // #swagger.responses[200] = { description: 'Logged in successfully!!' }

  // #swagger.responses[403] = { description: 'wrong email/password entered'}

  // #swagger.responses[400] = { description: 'Invalid information' },schema: { $ref: "#/definitions/ErrorMessage" }
);

export default router;
