import express from "express";
const router = express.Router();
import { wrapAsync, wrapAsyncMiddleware } from "../utils/asyncWrap.util";
import { isSignedin } from "../middlewares/auth.middlewares";
import {
  isCreator,
  getBookingById,
  hasBooked,
  bookingRegistrationSchemaValidator,
  hasTripEnded,
} from "../middlewares/booking.middlewares";

import {
  createBooking,
  getBooking,
  updateBooking,
  deleteBooking,
} from "../controllers/booking.controllers";

import { getBusById } from "../middlewares/bus.middlewares";
import { getTripById } from "../middlewares/trip.middlewares";

router.param("busId", wrapAsyncMiddleware(getBusById));
router.param("tripId", wrapAsyncMiddleware(getTripById));
router.param("bookingId", wrapAsyncMiddleware(getBookingById));

/**
@route    POST api/booking/busId/tripId
@desc     Book Ticket for a trip
@access   Public
*/

router.post(
  "/:busId/:tripId",
  wrapAsync(isSignedin),
  wrapAsync(isCreator),
  bookingRegistrationSchemaValidator,
  hasTripEnded,
  wrapAsync(createBooking)

  // #swagger.tags = ['Booking']
  // #swagger.summary = 'Returns message  on successful Booking'
  // #swagger.description = 'Books seat on bus for a particular trip'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the customer',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/CreateBooking" }
                  }
              }
          }
    */

  // #swagger.responses[201] = { description: 'Bus created successfully!' ,schema: { $ref: "#/definitions/BusTripBookingResponse" }}
  // #swagger.responses[400] = { description: 'Bus is full please select another bus' }
  // #swagger.responses[400] = { description: 'Cannot book this trip as it has already ended' }
  // #swagger.responses[400] = { description: 'Invalid details entered, cannot book seats' }
  // #swagger.responses[400] = {description: 'Authorization Error'}
  // #swagger.responses[403] = { description: 'you cannot book since you are the creator' }
);
/**
@route    GET api/booking/bookingId
@desc     Return booked details based on bookingId
@access   Public
*/
router.get(
  "/:bookingId",
  wrapAsync(isSignedin),
  hasBooked,
  getBooking

  // #swagger.tags = ['Booking']
  // #swagger.summary = 'Returns seat  booked information  based on booking ID'
  // #swagger.description = 'Returns seat  booked information  based on valid booking ID   and Authorization'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the customer',
        required: 'true',
  } */

  // #swagger.responses[200] =  {description: 'Returns requested bus based on id!',schema: { $ref: "#/definitions/GetBookingResponse" }}
  // #swagger.responses[400] = {description: 'Authorization Error'}
  // #swagger.responses[400] = { description: 'you haven't booked the ticket' }
);
/**
@route    PUT api/booking/bookingId
@desc     Update booked details based on bookingId
@access   Public
*/
router.put(
  "/:bookingId",
  wrapAsync(isSignedin),
  bookingRegistrationSchemaValidator,
  hasBooked,
  hasTripEnded,
  wrapAsync(updateBooking)

  // #swagger.tags = ['Booking']
  // #swagger.summary = 'Returns message  on successful update '
  // #swagger.description = 'Updates booked seats on bus for a particular trip based on valid booking ID   and Authorization'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the customer',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/CreateBooking" }
                  }
              }
          }
    */

  // #swagger.responses[200] = { description: 'Bus successfully updated' ,schema: { $ref: "#/definitions/BusTripBookingResponse" }}
  // #swagger.responses[400] = {description: 'Authorization Error'}
  // #swagger.responses[400] = { description: 'you haven't booked the ticket' }
  // #swagger.responses[400] = { description: 'Cannot cancel this trip as it has already ended' }
  // #swagger.responses[400] = { description: 'Cannot book this trip as it has already ended' }
  // #swagger.responses[400] = { description: 'you haven't booked the ticket' }
  // #swagger.responses[400] = { description: 'Invalid details entered, cannot book seats' }
);

/**
@route    DELETE api/booking/bookingId
@desc     Delete booked details based on bookingId
@access   Public
*/
router.delete(
  "/:bookingId",
  wrapAsync(isSignedin),
  hasBooked,
  wrapAsync(deleteBooking)

  // #swagger.tags = ['Booking']
  // #swagger.summary = 'Returns message  on successful deletion'
  // #swagger.description = 'Deletes the Booking information only if the authorization token and Booking id are valid'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the customer',
        required: 'true',
  } */

  // #swagger.responses[200] = { description: 'Bus successfully updated' ,schema: { $ref: "#/definitions/BusTripBookingResponse" }}
  // #swagger.responses[400] = {description: 'Authorization Error'}
  // #swagger.responses[400] = { description: 'you haven't booked the ticket' }
);

export default router;
