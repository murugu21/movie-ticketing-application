import express from "express";
const router = express.Router();
import { wrapAsync } from "../utils/asyncWrap.util";

import { isSignedin } from "../middlewares/auth.middlewares";
import {
  getBookingHistory,
  getUserById,
} from "../controllers/user.controllers";

/**
@route    GET api/user/
@desc     Returns  a User  
@access   Public
*/
router.get(
  "/",
  isSignedin,
  wrapAsync(getUserById)
  // #swagger.tags = ['User']
  // #swagger.summary = 'Returns  a User '
  // #swagger.description = 'Returns a  User  based on  Authorization'
  // #swagger.responses[200] = { description: 'Trip list based on query parameters',schema: { $ref: "#/definitions/GetUserResponse" } }
  // #swagger.responses[400] = {description: 'Authorization Error'}
);
/**
@route    GET api/user/bookings
@desc    Returns a  list of Bookings 
@access   Public
*/
router.get(
  "/bookings",
  isSignedin,
  wrapAsync(getBookingHistory)
  // #swagger.tags = ['User']
  // #swagger.summary = 'Returns a  list of Bookings '
  // #swagger.description = 'Returns a  list of Bookings  based on Authorization token'
  //  #swagger.parameters['status']={
  // description : "whether to get booked or cancelled tickets"
  // }
  // #swagger.responses[200] = { description: 'Trip list based on query parameters',schema: { $ref: "#/definitions/GetBookingListResponse" } }
  // #swagger.responses[400] = {description: 'Authorization Error'}
);

export default router;
