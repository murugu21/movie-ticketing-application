import express from "express";
const router = express.Router();
import { wrapAsync, wrapAsyncMiddleware } from "../utils/asyncWrap.util";

import { getBusById } from "../middlewares/bus.middlewares";
import {
  isSignedin,
  isOperator,
  isAuthenticatedToAccessTrip,
  isAuthenticatedToAccessBus,
} from "../middlewares/auth.middlewares";
import {
  tripRegistrationSchemaValidator,
  doesTripExists,
  getTripById,
  checkStartAndEndDate,
  isSeatsBooked,
  doDatesOverlap,
} from "../middlewares/trip.middlewares";
import {
  createTrip,
  getTrip,
  updateTrip,
  deleteTrip,
  getAllTrips,
  getStatus,
  getAllTripsRegisteredByOperator,
} from "../controllers/trip.controllers";
import { hasTripEnded } from "../middlewares/booking.middlewares";

router.param("busId", wrapAsyncMiddleware(getBusById));
router.param("tripId", wrapAsyncMiddleware(getTripById));
/**
@route    POST api/trip/busId
@desc     Creates a new trip 
@access   Public
*/
router.post(
  "/:busId",
  tripRegistrationSchemaValidator,
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  checkStartAndEndDate,
  wrapAsync(doesTripExists),
  wrapAsync(createTrip)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns message  on successful trip creation'
  // #swagger.description = 'Creates a new Trip document based on given data'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/RegisterTrip" }
                  }
              }
          }
    */

  // #swagger.responses[201] = { description: 'Trip created successfully!',schema: { $ref: "#/definitions/BusTripBookingResponse" } }
  // #swagger.responses[400] = { description: 'Invalid details entered, cannot create trip' }
  // #swagger.responses[400] = { description: 'Dates overlap with existing trip dates' }

  // #swagger.responses[400] = { description: 'Enter proper end/start date' }
  // #swagger.responses[400] = {description: 'Authorization Error'}

  // #swagger.responses[403] = {description: 'Customers don't have permission to create bus'}

  // #swagger.responses[403] = {description: 'Not authorizred to access or change bus details'}
);
/**
@route    GET api/trip/all
@desc     Returns a  list of trip that matches the query 
@access   Public
*/
router.get(
  "/all",
  wrapAsync(getAllTrips)
  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns a  list of trip that matches the query'
  // #swagger.description = 'Returns a  list of trip  based on query parameters'
  /* #swagger.parameters['from'] = {
        in: 'query',
        description: 'Pickup location of user',
        required: 'true',
  } */
  /* #swagger.parameters['to'] = {
        in: 'query',
        description: 'Drop location of user',
        required: 'true',
  } */
  /* #swagger.parameters['onDate'] = {
        in: 'query',
        description: 'Date of travel',
        required: 'true',
  } */
  /* #swagger.parameters['requireAc'] = {
        in: 'query',
        description: 'Only buses with AC',
        required: 'false',
  } */
  /* #swagger.parameters['requireAc'] = {
        in: 'query',
        description: 'Only buses with sleeper',
        required: 'false',
  } */

  // #swagger.responses[200] = { description: 'Trip list based on query parameters',schema: { $ref: "#/definitions/GetTripsListResponse" } }
);
/**
@route    GET api/trip/tripId
@desc     Returns a trip  based on trip ID
@access   Public
*/
router.get(
  "/:tripId",
  wrapAsync(isSignedin),
  wrapAsync(getTrip)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns  a trip  '
  // #swagger.description = 'Returns a trip  based on trip ID '

  // #swagger.responses[200] = { description: 'A trip document',schema: { $ref: "#/definitions/GetTripResponse" } }
  // #swagger.responses[400] = {description: 'Authorization Error'}
);
/**
@route    GET api/trip//operator/alltrips/
@desc     Returns a  list of  Trips 
@access   Public
*/
router.get(
  "/operator/alltrips/",
  wrapAsync(isSignedin),
  isOperator,
  wrapAsync(getAllTripsRegisteredByOperator)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns a  list of  Trips '
  // #swagger.description = 'Returns a  list of  Trips created by operator'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

  // #swagger.responses[200] = { description: 'Trips created by operator',schema: { $ref: "#/definitions/GetTripsListResponse" } }

  // #swagger.responses[400] = {description: 'Authorization Error'}

  // #swagger.responses[403] = {description: 'Customers don't have permission to create bus'}
);
/**
@route    PUT api/trip/:busId/:tripId
@desc     Updated a Trip  
@access   Public
*/
router.put(
  "/:busId/:tripId",
  tripRegistrationSchemaValidator,
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  isAuthenticatedToAccessTrip,
  wrapAsync(isSeatsBooked),
  wrapAsync(doDatesOverlap),
  checkStartAndEndDate,
  hasTripEnded,
  wrapAsync(updateTrip)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns message  on successful update'
  // #swagger.description = 'Updates the Trip with new values only if the authorization token and Trip id are valid'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */
  /* #swagger.requestBody = {
              required: true,
              content: {
                  "application/json": {
                      schema: { $ref: "#/definitions/RegisterTrip" }
                  }
              }
          }
    */
  // #swagger.responses[200] = { description: 'Trip updated successfully!',schema: { $ref: "#/definitions/BusTripBookingResponse" } }
  // #swagger.responses[400] = { description: 'Invalid details entered, cannot create trip' }
  // #swagger.responses[400] = { description: 'Dates overlap with existing trip dates' }

  // #swagger.responses[400] = { description: 'Enter proper end/start date' }
  // #swagger.responses[400] = {description: 'Authorization Error'}

  // #swagger.responses[403] = {description: 'Customers don't have permission to create bus'}

  // #swagger.responses[403] = {description: 'Not authorizred to access or change bus details'}
  // #swagger.responses[403] = {description: 'Not authorizred to access or change trip details'}
  // #swagger.responses[400] = { description: 'Cannot updated since trip got booked ' }
  // #swagger.responses[400] = { description: 'Cannot cancel this trip as it has already ended' }
);
/**
@route    DELETE api/trip/:busId/:tripId
@desc     Deletes a Trip  
@access   Public
*/
router.delete(
  "/:busId/:tripId",
  wrapAsync(isSignedin),
  isOperator,
  isAuthenticatedToAccessBus,
  isAuthenticatedToAccessTrip,
  wrapAsync(isSeatsBooked),
  hasTripEnded,
  wrapAsync(deleteTrip)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns message  on successful deletion'
  // #swagger.description = 'Deletes the Trip only if the authorization token and Trip id are valid'
  /* #swagger.parameters['Authorization'] = {
        in: 'header',
        description: 'Token issued to the operator',
        required: 'true',
  } */

  // #swagger.responses[200] = { description: 'Trip deleted successfully!',schema: { $ref: "#/definitions/BusTripBookingResponse" } }
  // #swagger.responses[400] = { description: 'Cannot cancel this trip as it has already ended' }
  // #swagger.responses[400] = {description: 'Authorization Error'}

  // #swagger.responses[403] = {description: 'Customers don't have permission to create bus'}

  // #swagger.responses[403] = {description: 'Not authorizred to access or change bus details'}
  // #swagger.responses[403] = {description: 'Not authorizred to access or change trip details'}
  // #swagger.responses[400] = { description: 'Cannot updated since trip got booked ' }
);
/**
@route    GET api/trip/status/:tripId
@desc     Returns  status of a trip 
@access   Public
*/
router.get(
  "/status/:tripId",
  wrapAsync(getStatus)

  // #swagger.tags = ['Trip']
  // #swagger.summary = 'Returns  status of a trip  '
  // #swagger.description = 'Returns a  status of a trip  based on trip ID '

  // #swagger.responses[200] = { description: 'Status of Trip',schema: { $ref: "#/definitions/GetTripStatusResponse" } }
);

export default router;
