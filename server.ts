import express, { Request, Response, json } from "express";
import authRoutes from "./routes/auth.routes";
import busRoutes from "./routes/bus.routes";
import bookingRoutes from "./routes/booking.routes";
import userRoutes from "./routes/user.routes";
import tripRoutes from "./routes/trip.routes";
import swaggerUi from "swagger-ui-express";
import {
  handleDatabaseError,
  handleDefaultError,
  handleMongooseError,
} from "./middlewares/errorHandlers.middleware";

import { readFile } from "fs/promises";

const app = express();

app.use(json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested, Content-Type, Accept Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, DELETE");
    return res.status(200).json({});
  }
  next();
});

app.use("/api/auth", authRoutes);
app.use("/api/bus", busRoutes);
app.use("/api/booking", bookingRoutes);
app.use("/api/user", userRoutes);
app.use("/api/trip", tripRoutes);

app.use(handleMongooseError);
app.use(handleDatabaseError);
app.use(handleDefaultError);

readFile("./dist/swagger_output.json", "utf8")
  .then((swaggerFile) => {
    app.use(
      "/api/documentation",
      swaggerUi.serve,
      swaggerUi.setup(JSON.parse(swaggerFile))
    );
  })
  .catch(() => {
    process.exit(1);
  });

app.get("/", (req: Request, res: Response) => {
  res.send("It works well");
});

export = app;
